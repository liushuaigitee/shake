package com.atswust.shake;

import com.atswust.shake.utils.JWTutils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@MapperScan(value = "com.atswust.shake.mapper")
public class ShakeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShakeApplication.class, args);
    }

}
