package com.atswust.shake.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * @author liushuai
 * 公共返回对象
 * @create 2022-02-06 11:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponBean <E>{
    private long code;
    private String message;
    private E data;
    public  static <E> ResponBean success(String message){
        ResponBean<E> responBean =new ResponBean<>();
        responBean.setCode(200);
        responBean.setMessage(message);
        return  responBean;
    }
    public  static <E> ResponBean success(String message, E data){
        ResponBean<E> responBean =new ResponBean<>();
        responBean.setCode(200);
        responBean.setMessage(message);
        responBean.setData(data);

        return  responBean;
    }

    public static <E> ResponBean error(String message,long code){
        return  new ResponBean<E>(code,message,null);
    }


    public static <E> ResponBean error(String message,E data) {
        return new ResponBean<E>(500, message, data);

    }

}