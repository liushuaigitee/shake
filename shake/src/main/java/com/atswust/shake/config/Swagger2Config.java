package com.atswust.shake.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author liushuai
 * @create 2022-02-03 14:31
 */

@Configuration
public class Swagger2Config {
    @Bean
    public Docket docket(){
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        ApiInfoBuilder apiInfoBuilder = new ApiInfoBuilder();
        ApiInfo apiInfo = apiInfoBuilder.title("摇一摇接口文档")
                .description("用于获取摇一摇相关数据")
                .version("1.0")
                .build();
        docket.select().apis(RequestHandlerSelectors.basePackage("com.atswust.shake.controller"))
                .paths(PathSelectors.any()).build();
        Docket docket1 = docket.apiInfo(apiInfo);
        return docket1;
    }
}
