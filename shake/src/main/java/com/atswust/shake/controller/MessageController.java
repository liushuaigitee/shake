package com.atswust.shake.controller;

import com.atswust.shake.config.ResponBean;
import com.atswust.shake.dto.SenderAndMessage;
import com.atswust.shake.pojo.Student;
import com.atswust.shake.service.MessageService;
import com.atswust.shake.service.StudentService;
import com.atswust.shake.utils.JWTutils;
import com.auth0.jwt.JWT;
import io.swagger.annotations.*;
import org.apache.catalina.connector.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liushuai
 * @create 2022-02-07 11:40
 */
@RestController
@CrossOrigin
@Api(tags="打招呼接口")
@RequestMapping("/message")
public class MessageController {
    @Autowired
    MessageService messageService;
    @Autowired
    StudentService studentService;
    @GetMapping("/sender")
    public ResponBean getSenderUser(HttpServletRequest request){
        String account = JWTutils.getAccountByToken(request.getHeader("token"));
        Integer id = studentService.getStuByAccount(account).getId();
        List<Student> sendAccountStu = messageService.getSendAccountStu(id);
        return ResponBean.success("查询成功",sendAccountStu);
    }
    @PostMapping("/send/{receiveId}")
    @ApiImplicitParams({@ApiImplicitParam(name = "receiveId",value = "接收者Id"),
                       @ApiImplicitParam(name="message",value = "发送的信息")})
    public ResponBean send(@PathVariable("receiveId") Integer receiveId,
                           @RequestParam("message") String message,
                           HttpServletRequest request){
        String account = JWTutils.getAccountByToken(request.getHeader("token"));
        messageService.sendMessage(studentService.getStuByAccount(account).getId(),message,receiveId);
        return ResponBean.success("发送成功");
    }
    @GetMapping("/senderAndMessage")
    @ApiOperation("返回发送者和其所发的消息")
    public ResponBean getSenderAndMessage(HttpServletRequest request){
        String account = JWTutils.getAccountByToken(request.getHeader("token"));
         Integer id = studentService.getStuByAccount(account).getId();
        List<SenderAndMessage> senderAndMessage = messageService.getSenderAndMessage(id);
        return  ResponBean.success("获取成功",senderAndMessage);
    }
}
