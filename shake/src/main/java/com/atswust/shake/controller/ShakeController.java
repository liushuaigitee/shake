package com.atswust.shake.controller;

import com.atswust.shake.config.ResponBean;
import com.atswust.shake.pojo.Student;
import com.atswust.shake.service.ShakeHistoryService;
import com.atswust.shake.service.StudentService;
import com.atswust.shake.utils.JWTutils;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author liushuai
 * @create 2022-02-07 10:39
 */
@RestController
@CrossOrigin
@Api(tags="摇一摇功能接口")
@RequestMapping("/shake")
public class ShakeController {
    @Autowired
    ShakeHistoryService shakeHistoryService;
    @Autowired
    StudentService studentService;
    @GetMapping("/oneStu")
    @ApiOperation(value="摇一摇返回一人")
    @ApiParam("不需要参数,只需将token放入请求头")
    public ResponBean shakeRetStu(HttpServletRequest request){
        String account = JWTutils.getAccountByToken(request.getHeader("token"));
        Student stu = studentService.getStuByAccount(account);
        //摇一摇返回一人
        Student stuByReturn = shakeHistoryService.shakeReturnStu(stu.getId());
        //更新摇一摇时间
        studentService.updateShakeTime(stu.getId());
        //保存摇一摇历史
        shakeHistoryService.saveShakeHistory(stu.getId(),stuByReturn.getId());
        //隐藏密码
        stuByReturn.setPassword(null);
        return  ResponBean.success("摇一摇成功",stuByReturn);
    }
    @GetMapping("/historyShake")
    @ApiOperation("返回摇一摇历史记录")
    public  ResponBean RetHistoryStu(HttpServletRequest request){
        String account = JWTutils.getAccountByToken(request.getHeader("token"));
        Student stuByAccount = studentService.getStuByAccount(account);
        List<Student> students = shakeHistoryService.retShakeHistory(stuByAccount.getId());
        return  ResponBean.success("成功",students);
    }
    @DeleteMapping("/all")
    @ApiOperation("删除摇一摇历史记录")
    public  ResponBean deleteAll(HttpServletRequest request){
        String account = JWTutils.getAccountByToken(request.getHeader("token"));
        shakeHistoryService.delAllHistory(studentService.getStuByAccount(account).getId());
       return ResponBean.success("删除成功");
    }
    @DeleteMapping("/{beShakerId}")
    @ApiOperation("删除某一条记录")
    public ResponBean deleteOne(HttpServletRequest request,@PathVariable("beShakerId") Integer beShakerId){
        String account = JWTutils.getAccountByToken(request.getHeader("token"));
        shakeHistoryService.delByBeShakerId(studentService.getStuByAccount(account).getId(),beShakerId);
       return ResponBean.success("删除成功");
    }
    @PostMapping("/abc")
    public  ResponBean dele(@RequestHeader("token")String token){
        return  ResponBean.success(token);
    }

}
