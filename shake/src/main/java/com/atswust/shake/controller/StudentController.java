package com.atswust.shake.controller;

import com.atswust.shake.config.ResponBean;
import com.atswust.shake.pojo.Student;
import com.atswust.shake.service.StudentService;
import com.atswust.shake.utils.JWTutils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liushuai
 * @create 2022-02-06 14:59
 */
@RestController
@RequestMapping("/student")
@Api(tags="学生注册登录接口")
@CrossOrigin
public class StudentController {
    @Autowired
    StudentService studentService;
    @PostMapping("/register")
    @ApiOperation(value="注册接口" )
    public ResponBean registerStu(@RequestBody  @ApiParam(value = "学生实体类") Student student, HttpServletResponse response){
        System.out.println(student.getAccount());
        boolean b = studentService.addUser(student);
        if(b){
            student.setPassword(null);
            return ResponBean.success("200",student);
        }else{
            response.setStatus(403);
            return  ResponBean.error("注册失败,学号重复",403);
        }
    }
    @ApiOperation(value = "登录接口")
    @PostMapping("/login")
    @CrossOrigin
    public  ResponBean loginStu(@ApiParam(name="account" ,value = "账号") String account,
                                @ApiParam(name="password" ,value = "密码")String password,
                                HttpServletResponse response){
        boolean ok = studentService.login(account, password);
        Map<String,String> map =new HashMap<>();
        map.put("account",account);
        if(ok){
            String token = JWTutils.getToken(map);
            return ResponBean.success("登陆成功",token);
        }else{
            response.setStatus(403);
            return ResponBean.error("请检查账号密码",403);
        }
    }
    @ApiOperation(value="退出接口,主要前端处理")
    @GetMapping("/logout")
    public  ResponBean logStu(HttpServletRequest request){
       return ResponBean.success("退出成功");
    }
    @GetMapping("/hello")
    public  String hello(){
        return "hello";
    }
}
