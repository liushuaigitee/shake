package com.atswust.shake.interceptor;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.web.servlet.HandlerInterceptor;
import springfox.documentation.spring.web.json.Json;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SAAJResult;
import java.io.BufferedReader;
import java.nio.CharBuffer;
import java.util.Map;

/**
 * @author liushuai
 * @create 2022-03-18 22:50
 */

public class RegisterInteceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        final BufferedReader reader = request.getReader();
        String s = null;
        StringBuffer stringBuffer = new StringBuffer();
        while ((s= reader.readLine())!=null){
            stringBuffer.append(s);
        }
        final String s1 = stringBuffer.toString();
        System.out.println(stringBuffer);
        final ObjectMapper objectMapper = new ObjectMapper();
        final Map<String, Object> stringObjectMap = objectMapper.readValue(s1, new TypeReference<Map<String, Object>>() {
        });
        final String account = (String) stringObjectMap.get("account");
        if(account.length()!= 10){
            throw  new Exception("学号错误,长度不为10");
        }else {
            return  true;
        }
    }

}
