package com.atswust.shake.mapper;

import com.atswust.shake.dto.SenderAndMessage;
import com.atswust.shake.pojo.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author 86151
* @description 针对表【message】的数据库操作Mapper
* @createDate 2022-02-07 11:30:30
* @Entity com.atswust.shake.pojo.Message
*/
public interface MessageMapper extends BaseMapper<Message> {
    public List<String> getSendAccountId(@Param("receiveId") Integer receiveId);
    public  List<String> getReceiveMessage(Integer id);
    public  List<SenderAndMessage > getSenderAndMessage(Integer id);
}




