package com.atswust.shake.mapper;

import com.atswust.shake.pojo.ShakeHistory;
import com.atswust.shake.pojo.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author 86151
* @description 针对表【shake_history】的数据库操作Mapper
* @createDate 2022-02-06 19:52:41
* @Entity com.atswust.shake.pojo.ShakeHistory
*/
public interface ShakeHistoryMapper extends BaseMapper<ShakeHistory> {
        List<Student> selectHistoryShake(Integer id);
}




