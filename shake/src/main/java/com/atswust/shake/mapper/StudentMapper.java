package com.atswust.shake.mapper;

import com.atswust.shake.pojo.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86151
* @description 针对表【student】的数据库操作Mapper
* @createDate 2022-02-06 13:58:10
* @Entity com.atswust.shake.pojo.Student
*/
public interface StudentMapper extends BaseMapper<Student> {

}




