package com.atswust.shake.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * @TableName message
 */
@TableName(value ="message")
@Data
@AllArgsConstructor
public class Message implements Serializable {
    /**
     * 
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    /**
     * 学生接收方id
     */
    private Integer receiveId;

    /**
     * 发送者id
     */
    private Integer sendId;

    /**
     * 打招呼信息
     */
    private String message;

    /**
     * 接收时间
     */
    private Date receiveTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}