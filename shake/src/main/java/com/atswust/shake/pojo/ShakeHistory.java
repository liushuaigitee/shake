package com.atswust.shake.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * @TableName shake_history
 */
@TableName(value ="shake_history")
@Data
@AllArgsConstructor
public class ShakeHistory  {
    /**
     * 
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 摇动者的学生id
     */
    private Integer shakerId;
    /**
     * 被摇到的人的id
     */
    private Integer beShakerId;

}