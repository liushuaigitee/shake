package com.atswust.shake.service;

import com.atswust.shake.dto.SenderAndMessage;
import com.atswust.shake.pojo.Message;
import com.atswust.shake.pojo.Student;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
* @author 86151
* @description 针对表【message】的数据库操作Service
* @createDate 2022-02-07 11:30:30
*/
public interface MessageService extends IService<Message> {
    void sendMessage(Integer sendId,String message,Integer receiverId);
    List<Student> getSendAccountStu(Integer id);
    public List<SenderAndMessage> getSenderAndMessage(Integer id);
}
