package com.atswust.shake.service;

import com.atswust.shake.pojo.ShakeHistory;
import com.atswust.shake.pojo.Student;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 86151
* @description 针对表【shake_history】的数据库操作Service
* @createDate 2022-02-06 19:52:41
*/
public interface ShakeHistoryService extends IService<ShakeHistory> {
        //摇一摇随机返回一个人
        Student shakeReturnStu(Integer id);
        //保存摇一摇历史记录
        void    saveShakeHistory(Integer shakerId ,Integer beShakerId);
        //返回摇一摇历史
        List<Student> retShakeHistory(Integer id);
        //删除历史记录
        void delAllHistory(Integer shakerId);
        //删除某一个历史记录
        void delByBeShakerId(Integer shakerId,Integer beShakerId);
}
