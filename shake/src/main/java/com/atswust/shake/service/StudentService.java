package com.atswust.shake.service;

import com.atswust.shake.pojo.Student;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
* @author 86151
* @description 针对表【student】的数据库操作Service
* @createDate 2022-02-06 13:58:10
*/
public interface StudentService extends IService<Student> {
         boolean addUser(Student student);
         boolean login(String account,String password);
         void    updateShakeTime(Integer id );
         Student getStuByAccount(String account);
}
