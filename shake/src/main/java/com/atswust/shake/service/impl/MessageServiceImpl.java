package com.atswust.shake.service.impl;

import com.atswust.shake.dto.SenderAndMessage;
import com.atswust.shake.mapper.StudentMapper;
import com.atswust.shake.pojo.Student;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atswust.shake.pojo.Message;
import com.atswust.shake.service.MessageService;
import com.atswust.shake.mapper.MessageMapper;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
* @author 86151
* @description 针对表【message】的数据库操作Service实现
* @createDate 2022-02-07 11:30:30
*/
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message>
    implements MessageService{
    @Autowired
    MessageMapper messageMapper;
    @Autowired
    StudentMapper studentMapper;
    @Override
    public void sendMessage(Integer sendId, String message, Integer receiverId) {
        Message message1=new Message(null,receiverId,sendId,message,new Date(System.currentTimeMillis()));
        messageMapper.insert(message1);
    }

    @Override
    public List<Student> getSendAccountStu(Integer id) {
        List<String> sendAccountId = messageMapper.getSendAccountId(id);
        List<Student> students = new ArrayList<>();
        for (String s : sendAccountId) {
            QueryWrapper<Student> studentQueryWrapper =new QueryWrapper<>();
            QueryWrapper<Student> account = studentQueryWrapper.eq("account", s);
            Student student = studentMapper.selectOne(account);
            students.add(student);
        }
        students.forEach(s->{s.setPassword(null);});
        return students;
    }

    public  List<SenderAndMessage> getSenderAndMessage(Integer id){
        final List<SenderAndMessage> senderAndMessage = messageMapper.getSenderAndMessage(id);
        return  senderAndMessage;
    }

}




