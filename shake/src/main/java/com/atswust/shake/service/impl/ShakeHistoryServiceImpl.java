package com.atswust.shake.service.impl;

import com.atswust.shake.mapper.StudentMapper;
import com.atswust.shake.pojo.Student;
import com.auth0.jwt.JWT;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atswust.shake.pojo.ShakeHistory;
import com.atswust.shake.service.ShakeHistoryService;
import com.atswust.shake.mapper.ShakeHistoryMapper;
import javafx.stage.Popup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.HTMLDocument;
import java.util.*;

/**
* @author 86151
* @description 针对表【shake_history】的数据库操作Service实现
* @createDate 2022-02-06 19:52:41
*/
@Service
public class ShakeHistoryServiceImpl extends ServiceImpl<ShakeHistoryMapper, ShakeHistory>
    implements ShakeHistoryService{

    @Autowired
    ShakeHistoryMapper shakeHistoryMapper;
    @Autowired
    StudentMapper studentMapper;
    @Autowired
    private MybatisPlusInterceptor mybatisPlusInterceptor;
    //摇一摇返回一个人
    @Override
    public Student shakeReturnStu(Integer id) {
        IPage<Student> page = new Page<>();
        page.setSize(10);
        page.setCurrent(1);
        QueryWrapper<Student> queryWrapper=new QueryWrapper<>();
        queryWrapper.ne("id",id);
        queryWrapper.orderByAsc("shake_time");
        IPage<Student> studentIPage = studentMapper.selectPage(page, queryWrapper);
        Random random = new Random();
        List<Student> records = studentIPage.getRecords();
        int bound=10>records.size()?records.size():10;
        int randomStu = random.nextInt(bound);
        Student student = records.get(randomStu);
        return student;
    }

    @Override
    public void saveShakeHistory(Integer shakerId, Integer beShakerId) {
        ShakeHistory shakeHistory = new ShakeHistory(null,shakerId,beShakerId);
        shakeHistoryMapper.insert(shakeHistory);
    }

    @Override
    public List<Student> retShakeHistory(Integer id) {
         List<Student> students = shakeHistoryMapper.selectHistoryShake(id);
            students.forEach(s->{s.setPassword(null);});
        return students;
    }

    @Override
    public void delAllHistory(Integer shakerId) {
       QueryWrapper<ShakeHistory> queryWrapper = new QueryWrapper<>();
        int delete = shakeHistoryMapper.delete(queryWrapper.eq("shaker_id",shakerId));
    }

    @Override
    public void delByBeShakerId(Integer shakerId, Integer beShakerId) {
        QueryWrapper<ShakeHistory> queryWrapper = new QueryWrapper<>();
        int delete = shakeHistoryMapper.delete(queryWrapper.eq("shaker_id", shakerId).eq("be_shaker_id", beShakerId));
    }
}




