package com.atswust.shake.service.impl;

import com.atswust.shake.utils.PasswordUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atswust.shake.pojo.Student;
import com.atswust.shake.service.StudentService;
import com.atswust.shake.mapper.StudentMapper;
import net.sf.jsqlparser.statement.select.Select;
import org.apache.commons.codec.digest.Md5Crypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.security.provider.MD5;

import java.nio.charset.StandardCharsets;
import java.sql.Wrapper;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @author 86151
* @description 针对表【student】的数据库操作Service实现
* @createDate 2022-02-06 13:58:10
*/
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student>
    implements StudentService{
    @Autowired
    private StudentMapper mapper;
    @Override
    public boolean addUser(Student student) {
        String account = student.getAccount();
        try {
            student.setPassword(PasswordUtils.shaEncode(student.getPassword()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String,Object> map=new HashMap<>();
        map.put("account",account);
        List<Student> students = mapper.selectByMap(map);
        System.out.println(students.size());
        if(students.size()==0){
            mapper.insert(student);
            return true;
        }else{
            return  false;
        }
    }

    @Override
    public boolean login(String account, String password) {
        QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
        try {
            queryWrapper.eq("account",account)
                        .eq("password",PasswordUtils.shaEncode(password));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Md5Crypt.md5Crypt(password.getBytes(StandardCharsets.UTF_8)));
        Student student = mapper.selectOne(queryWrapper);
        if(student==null){
            return  false;
        }else{
            return true;
        }
    }

    @Override
    public void updateShakeTime(Integer id) {
        Student student = mapper.selectById(id);
        student.setShakeTime(new Date(System.currentTimeMillis()));
        mapper.updateById(student);
    }

    @Override
    public Student getStuByAccount(String account) {
        QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account",account);
        return mapper.selectOne(queryWrapper);
    }
}




