package com.liushuai.test;

/**
 * @author liushuai
 * @create 2022-04-02 14:07
 */

public interface ArearInterface {
    //数组存放计算所需数据
    double calculate(double[] data);
}

class Triangle implements  ArearInterface{
    private double high ;
    private double base;

    public Triangle(double high, double base) {
        this.high = high;
        this.base = base;
    }
    @Override
    public double calculate(double[] data) {
        double arear = 1.0;
        for(int i =0;i<data.length ;i++ ){
            arear *= data[i];
        }
        return arear/2;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }
}
class Rectangle implements  ArearInterface{
    private  double  length;
    private  double  width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double calculate(double[] data) {
        double arear = 1.0;
        for(int i =0;i<data.length ;i++ ){
            arear *= data[i];
        }
        return arear;
    }
}
class Circle implements ArearInterface {
    private  double  radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculate(double[] data) {
        return Math.PI *data[0]*data[0];
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}

class  TestInterface {
    public static void main(String[] args) {
        //三角形
        final Triangle triangle = new Triangle(2.0, 1.0);
        final double calculate = triangle.calculate(new double[]{triangle.getBase(),triangle.getHigh()});
        System.out.println("三角形的面积为:"+calculate);
        //矩形
        final Rectangle rectangle = new Rectangle(2.0, 3.0);
        final double calculate1 = rectangle.calculate(new double[]{rectangle.getLength(), rectangle.getWidth()});
        System.out.println("矩形的面积为"+calculate1);
        //圆
        final Circle circle = new Circle(1.0);
        final double calculate2 = circle.calculate(new double[]{circle.getRadius()});
        System.out.println("圆的面积为:"+calculate2);
    }
}